<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale_product`.
 */
class m190118_123501_create_sale_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sale_product}}', [
            'id' => $this->integer()->notNull(),
            'quantity' => $this->double()->notNull()->defaultValue(0),
            'quantity_reserved' => $this->double()->defaultValue(0),
            'available' => $this->char(1),
            'measure' => $this->integer(),
            'weight' => $this->double()->notNull()->defaultValue(0),
            'width' => $this->double(),
            'height' => $this->double(),
            'length' => $this->double()

        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-sale_product}}', '{{%sale_product}}', 'id');
        $this->addForeignKey('{{%fk-sale_product-element}}', '{{%sale_product}}', 'id', '{{%reference_element}}','id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_product}}');
    }
}
