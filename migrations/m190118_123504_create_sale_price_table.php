<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale_price`.
 */
class m190118_123504_create_sale_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sale_price_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'base' => $this->string(1)->notNull()->defaultValue('N'),
            'sort' => $this->integer()->notNull()->defaultValue(100),
            'external_id' => $this->string(255),
            'created_by' => $this->integer(),
            'modified_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

        $this->addForeignKey('{{%fk-sale_price_type-created}}', '{{%sale_price_type}}', 'created_by', '{{%base_user}}', 'id', 'SET NULL');
        $this->addForeignKey('{{%fk-sale_price_type-modified}}', '{{%sale_price_type}}', 'modified_by', '{{%base_user}}', 'id', 'SET NULL');

        $this->insert('{{%sale_price_type}}', [
            'id' => 1,
            'name' => Yii::t('app', 'Base price'),
            'base' => 'Y',
            'sort' => 100,
            'external_id' => 'price',
            'created_by' => 1,
            'modified_by' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_price}}');
    }
}
