<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale_product_price`.
 */
class m190118_123505_create_sale_product_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sale_product_price}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'price_type' => $this->integer()->notNull(),
            'price' => $this->decimal(18,2)->notNull(),
            'currency' => $this->char(3)->notNull()
        ], $tableOptions);

        $this->createIndex('{{%idx-sale_product_price-pid}}', '{{%sale_product_price}}', ['product_id', 'price_type']);
        $this->createIndex('{{%idx-sale_product_price-type}}', '{{%sale_product_price}}', 'price_type');

        $this->addForeignKey('{{%fk-sale_product_price-product}}', '{{%sale_product_price}}', 'product_id', '{{%sale_product}}', 'id', 'CASCADE');
        $this->addForeignKey('{{%fk-sale_product_price-type}}', '{{%sale_product_price}}', 'price_type', '{{%sale_price_type}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_product_price}}');
    }
}
