<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale_currency`.
 */
class m190118_123503_create_sale_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sale_currency}}', [
            'id' => $this->string(3)->notNull(),
            'base' => $this->string(1)->notNull()->defaultValue('N'),
            'amount_cnt' => $this->integer()->notNull()->defaultValue(1),
            'amount' => $this->decimal(18, 4)->defaultValue(1),
            'sort' => $this->integer()->notNull()->defaultValue(100),
            'created_by' => $this->integer(18),
            'modified_by' => $this->integer(18),
            'current_base_rate' => $this->decimal(26, 12)
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-sale_currency}}', '{{%sale_currency}}', 'id');

        $this->insert('{{%sale_currency}}', [
            'id' => 'RUB',
            'base' => 'Y',
            'amount_cnt' => 1,
            'amount' => 1,
            'sort' => 100,
            'created_by' => 1,
            'modified_by' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_currency}}');
    }
}
