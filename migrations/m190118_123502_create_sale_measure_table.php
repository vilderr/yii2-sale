<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale_measure`.
 */
class m190118_123502_create_sale_measure_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sale_measure}}', [
            'id' => $this->primaryKey(),
            'code' => $this->integer()->notNull(),
            'title' => $this->string(500)->notNull(),
            'symbol_rus' => $this->string(20),
            'symbol_intl' => $this->string(20),
            'symbol_letter_intl' => $this->string(20),
            'default' => $this->char(1)->defaultValue('N')
        ]);

        $this->createIndex('{{%idx-sale_measure-code}}', '{{%sale_measure}}', 'code', true);

        $this->batchInsert(
            '{{%sale_measure}}',
            ['code', 'title', 'symbol_rus', 'symbol_intl', 'symbol_letter_intl', 'default'],
            [
                [6, 'Метр', 'м', 'm', 'MTR', 'N'],
                [112, 'Литр', 'л', 'l', 'LTR', 'N'],
                [163, 'Грамм', 'г', 'g', 'GRM', 'N'],
                [166, 'Килограмм', 'кг', 'kg', 'KGM', 'N'],
                [168, 'Тонна. Метрическая тонна (1000 кг)', 'т', 't', 'TNE', 'N'],
                [796, 'Штука', 'шт', 'pc. 1', 'PCE. NMB', 'Y']
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sale_measure}}');
    }
}
