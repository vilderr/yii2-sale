<?php

namespace vilderr\sale\controllers\backend\currencies;

use vilderr\main\web\Controller;

/**
 * Class RatesController
 * @package vilderr\sale\controllers\backend\currencies
 */
class RatesController extends Controller
{
    public function actionIndex()
    {
        $this->view->title = $this->module->name . ': Курсы валют';

        $this->view->params['breadcrumbs'][] = [
            'label' => $this->module->name,
            'url' => ['/sale/']
        ];
        $this->view->params['breadcrumbs'][] = 'Курсы валют';

        return $this->render('index');
    }
}