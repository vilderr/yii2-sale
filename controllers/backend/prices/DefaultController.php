<?php

namespace vilderr\sale\controllers\backend\prices;

use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use vilderr\main\Yii;
use vilderr\main\web\Controller;
use vilderr\main\base\Result;
use vilderr\main\base\Error;
use vilderr\sale\collects\price\SearchCollection;
use vilderr\sale\models\price\PriceType;

/**
 * Class DefaultController
 * @package vilderr\sale\controllers\backend\prices
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        $model = new SearchCollection();
        $data = $model->search(Yii::$app->request->queryParams);

        $this->view->title = $this->module->name . ': Типы цен';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->module->name,
            'url' => ['/sale/']
        ];
        $this->view->params['breadcrumbs'][] = 'Типы цен';

        return $this->render('index', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $model = new PriceType();
        $model->loadDefaultValues();
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post(), 'price_type')) {
            $result = new Result();
            $transaction = $model::getDb()->beginTransaction();

            try {
                $res = $model->insert();
                $errIndex = 1;
                if ($res === false) {
                    $result->addError(new Error('Ошибка при сохранении типа цены'), $errIndex);
                    foreach ($model->getErrorSummary(true) as $error) {
                        $errIndex++;
                        $result->addError(new Error($error), $errIndex);
                    }
                }

                if ($result->isSuccess()) {
                    $transaction->commit();

                    $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));
                    switch (Yii::$app->request->post('action', 'save')) {
                        case 'save':
                            return $this->redirect($returnUrl);
                        default:
                            return $this->redirect([
                                'update',
                                'id' => $model->id,
                                'returnUrl' => $returnUrl,
                            ]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', implode('<br>', $result->getErrorMessages()), false);

                    $transaction->rollBack();
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        $this->view->title = $this->module->name . ': Новый тип цен';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->module->name,
            'url' => ['/sale/']
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Типы цен',
            'url' => ['index']
        ];

        $this->view->params['breadcrumbs'][] = 'Новый тип цен';

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post(), 'price_type')) {
            $result = new Result();
            $transaction = $model::getDb()->beginTransaction();

            try {
                $res = $model->update();
                $errIndex = 1;
                if ($res === false) {
                    $result->addError(new Error('Ошибка при сохранении типа цены'), $errIndex);
                    foreach ($model->getErrorSummary(true) as $error) {
                        $errIndex++;
                        $result->addError(new Error($error), $errIndex);
                    }
                }

                if ($result->isSuccess()) {
                    $transaction->commit();

                    $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));
                    switch (Yii::$app->request->post('action', 'save')) {
                        case 'save':
                            return $this->redirect($returnUrl);
                        default:
                            return $this->redirect([
                                'update',
                                'id' => $model->id,
                                'returnUrl' => $returnUrl,
                            ]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', implode('<br>', $result->getErrorMessages()), false);

                    $transaction->rollBack();
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        $this->view->title = 'Редактирование типа цен #' . $model->id;
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->module->name,
            'url' => ['/sale/']
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Типы цен',
            'url' => ['index']
        ];

        $this->view->params['breadcrumbs'][] = $model->name;

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        /**************** запись в БД ********************/
        $transaction = $model::getDb()->beginTransaction();
        try {
            if (false === $model->delete()) {
                Yii::$app->session->setFlash('error', 'Ошибка удаления типа цены');
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }
        /*************************************************/

        $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));

        return $this->redirect($returnUrl);
    }

    /**
     * @param $id
     *
     * @return null|PriceType
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if ($model = PriceType::findOne($id)) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена');
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST']
                ],
            ],
        ];
    }
}