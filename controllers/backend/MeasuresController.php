<?php

namespace vilderr\sale\controllers\backend;

use vilderr\main\base\Result;
use vilderr\main\base\Error;
use vilderr\main\web\Controller;
use vilderr\main\Yii;
use vilderr\sale\models\Measure;
use yii\data\ActiveDataProvider;
use yii\db;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\filters\VerbFilter;

/**
 * Class MeasuresController
 * @package vilderr\sale\controllers\backend
 */
class MeasuresController extends Controller
{
    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => Measure::find()
        ]);

        $this->view->title = $this->module->name . ': Единицы измерения';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->module->name,
            'url' => ['/sale/']
        ];
        $this->view->params['breadcrumbs'][] = 'Единицы измерения';

        return $this->render('index', [
            'data' => $data
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $model = new Measure();
        $model->loadDefaultValues();
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post(), 'measure')) {
            $result = new Result();
            $transaction = $model::getDb()->beginTransaction();

            try {
                $res = $model->insert();
                $errIndex = 1;
                if ($res === false) {
                    $result->addError(new Error('Ошибка при сохранении единицы измерения'), $errIndex);
                    foreach ($model->getErrorSummary(true) as $error) {
                        $errIndex++;
                        $result->addError(new Error($error), $errIndex);
                    }
                }

                if ($result->isSuccess()) {
                    $transaction->commit();

                    $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));
                    switch (Yii::$app->request->post('action', 'save')) {
                        case 'save':
                            return $this->redirect($returnUrl);
                        default:
                            return $this->redirect([
                                'update',
                                'id' => $model->id,
                                'returnUrl' => $returnUrl,
                            ]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', implode('<br>', $result->getErrorMessages()), false);

                    $transaction->rollBack();
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        $this->view->title = $this->module->name . ': Новая единица измерения';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->module->name,
            'url' => ['/sale/']
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Единицы измерения',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = 'Новая единица измерения';

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \Throwable
     * @throws db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post(), 'measure')) {
            $result = new Result();
            $transaction = $model::getDb()->beginTransaction();

            try {
                $res = $model->update();
                $errIndex = 1;
                if ($res === false) {
                    $result->addError(new Error('Ошибка при сохранении единицы измерения'), $errIndex);
                    foreach ($model->getErrorSummary(true) as $error) {
                        $errIndex++;
                        $result->addError(new Error($error), $errIndex);
                    }
                }

                if ($result->isSuccess()) {
                    $transaction->commit();

                    $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));
                    switch (Yii::$app->request->post('action', 'save')) {
                        case 'save':
                            return $this->redirect($returnUrl);
                        default:
                            return $this->redirect([
                                'update',
                                'id' => $model->id,
                                'returnUrl' => $returnUrl,
                            ]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', implode('<br>', $result->getErrorMessages()), false);

                    $transaction->rollBack();
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        $this->view->title = $this->module->name . ': Редактирование единицы измерения';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->module->name,
            'url' => ['/sale/']
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Единицы измерения',
            'url' => ['index']
        ];
        $this->view->params['breadcrumbs'][] = $model->title;

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws db\Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        /**************** запись в БД ********************/
        $transaction = $model::getDb()->beginTransaction();
        try {
            if (false === $model->delete()) {
                Yii::$app->session->setFlash('error', 'Ошибка удаления единицы измерения');
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }
        /*************************************************/

        $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));

        return $this->redirect($returnUrl);
    }

    /**
     * @param $id
     *
     * @return null|Measure
     * @throws db\Exception
     */
    protected function findModel($id)
    {
        if ($model = Measure::findOne($id)) {
            return $model;
        }

        throw new db\Exception('Measure not found');
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST']
                ],
            ],
        ];
    }
}