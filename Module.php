<?php

namespace vilderr\sale;

use vilderr\main\Yii;
use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package vilderr\sale
 */
class Module extends \vilderr\main\base\Module implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations["modules/sale/*"] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@vendor/vilderr/sale/messages',
            'fileMap' => [
                'modules/sale/module' => 'module.php',
            ],
        ];

        if ($app->id == 'backend') {
            $app->urlManager->addRules([

                [
                    'class' => 'yii\\web\\GroupUrlRule',
                    'prefix' => 'sale',
                    'routePrefix' => 'sale',
                    'rules' => [
                        'prices' => 'prices/default/index',
                        'prices/<_a:(create|update|delete)>' => 'prices/default/<_a>',
                        'currencies' => 'currencies/default/index',
                        'currencies/<_a:(create|update|delete)>' => 'currencies/default/<_a>',
                        'currencies/rates' => 'currencies/rates/index',
                        'measures' => 'measures/index',
                        'measures/<_a:(create|update|delete)>' => 'measures/<_a>',
                    ]
                ],
            ]);
        }
    }

    public static function t($message, $category = 'module', $params = [], $language = null)
    {
        return Yii::t('modules/sale/' . $category, $message, $params, $language);
    }

    public function getName()
    {
        return self::t('Sale module');
    }

    public function getMenuItems()
    {
        $items = [
            'settings' => [
                'items' => [
                    'modules' => [
                        'items' => [
                            300 => [
                                'label' => 'Магазин',
                                'url' => ['/main/settings/default/index', 'module_id' => 'sale'],
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $items;
    }
}