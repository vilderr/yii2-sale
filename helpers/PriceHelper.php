<?php

namespace vilderr\sale\helpers;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use vilderr\main\helpers\ModelHelper;

class PriceHelper extends ModelHelper
{
    const STATUS_ACTIVE = 'Y';
    const STATUS_WAIT = 'N';

    public static function statusList()
    {
        return [
            PriceHelper::STATUS_ACTIVE => 'Да',
            PriceHelper::STATUS_WAIT => 'Нет',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case PriceHelper::STATUS_WAIT:
                $class = 'badge badge-light';
                break;
            case PriceHelper::STATUS_ACTIVE:
                $class = 'badge badge-success';
                break;
            default:
                $class = 'badge badge-light';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}