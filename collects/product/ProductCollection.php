<?php

namespace vilderr\sale\collects\product;

use vilderr\main\base\Collection;
use vilderr\sale\models\price\PriceType;
use vilderr\sale\models\Product;

/**
 * Class Collection
 * @package vilderr\sale\collects\product
 *
 * @property Product $product
 */
class ProductCollection extends Collection
{
    public function __construct(Product $product = null, array $config = [])
    {
        if ($product) {
            $this->product = $product;
        } else {
            $this->product = new Product();
        }

        $this->prices = array_map(function () {

        }, PriceType::find()->orderBy(['base' => SORT_DESC])->indexBy('id')->all());

        parent::__construct($config);
    }

    public function internalCollections()
    {
        return ['product', 'prices'];
    }
}