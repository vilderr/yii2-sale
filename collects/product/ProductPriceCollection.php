<?php

namespace vilderr\sale\collects\product;

use yii\base\Model;

/**
 * Class ProductPriceCollection
 * @package vilderr\sale\collects\product
 */
class ProductPriceCollection extends Model
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}