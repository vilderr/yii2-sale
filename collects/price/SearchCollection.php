<?php

namespace vilderr\sale\collects\price;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use vilderr\sale\models\price\PriceType;

/**
 * Class SearchCollection
 * @package vilderr\catalog\collects\price
 */
class SearchCollection extends Model
{
    public $id;
    public $name;
    public $external_id;
    public $base;
    public $sort;

    public function rules()
    {
        return [
            [['id', 'sort'], 'integer'],
            [['name', 'external_id', 'email', 'base'], 'safe'],
        ];
    }

    public function search(array $params): ActiveDataProvider
    {
        $query = PriceType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sort' => $this->sort,
            'base' => $this->base
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'external_id', $this->external_id]);

        return $dataProvider;
    }
}