<?php

namespace vilderr\sale\behaviors;

use yii\base\Behavior;
use yii\base\ModelEvent;
use yii\db\ActiveRecord;
use vilderr\sale\models\price\PriceType;
use yii\base\Exception;

/**
 * Class PriceBehavior
 * @package vilderr\catalog\behaviors
 *
 * @property PriceType $owner
 */
class PriceTypeBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @param ModelEvent $event
     */
    public function beforeSave(ModelEvent $event)
    {
        if ($this->owner->base == 'Y')
            $this->owner->updateAll(['base' => 'N'], $this->owner->find()->where(['base' => 'Y'])->where);
        else {
            if ($this->owner->find()->where(['base' => 'Y'])->count() <= 0) {
                $event->isValid = false;
                $this->owner->addError('system', 'Должен быть хотя бы один базовый тип цен');
            }
        }
    }

    /**
     * @param ModelEvent $event
     */
    public function beforeUpdate(ModelEvent $event)
    {
        $count = $this->owner->find()->count();

        if ($this->owner->base == 'Y')
            $this->owner->updateAll(['base' => 'N'], $this->owner->find()->andWhere(['base' => 'Y'])->andWhere(['<>', 'id', $this->owner->id])->where);
        else {
            if ($count == 1) {
                $event->isValid = false;
                $this->owner->addError('system', 'Должен быть хотя бы один базовый тип цен');
            } else {
                if ($this->owner->find()->where(['base' => 'Y'])->andWhere(['<>', 'id', $this->owner->id])->count() <= 0) {
                    $event->isValid = false;
                    $this->owner->addError('system', 'Должен быть хотя бы один базовый тип цен');
                }
            }
        }
    }

    /**
     * @param ModelEvent $event
     */
    public function beforeDelete(ModelEvent $event)
    {
        if ($this->owner->base == 'Y') {
            if ($this->owner->find()->andWhere(['base' => 'Y'])->andWhere(['<>', 'id', $this->owner->id])->count() <= 0) {
                $event->isValid = false;
                $this->owner->addError('system', 'Должен быть хотя бы один базовый тип цен');
            }
        }
    }
}