<?php

namespace vilderr\sale\behaviors;

use vilderr\sale\models\Product;
use vilderr\sale\models\Price;
use yii\base\Behavior;

/**
 * Class ProductBehavior
 * @package vilderr\sale\behaviors
 *
 * @property Product $owner
 */
class ProductBehavior extends Behavior
{
    public $prices;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->owner->hasMany(Price::class, ['product_id' => 'id'])->indexBy('price_type');
    }
}