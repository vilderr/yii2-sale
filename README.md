Sale module
===========
Sale module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vilderr/sale "*"
```

or add

```
"vilderr/sale": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \vilderr\sale\AutoloadExample::widget(); ?>```