<?php

namespace vilderr\sale\models\price;

use yii\db\ActiveQuery;
use vilderr\main\helpers\ModelHelper;
use vilderr\sale\models\price\PriceType;

/**
 * Class PriceQuery
 * @package vilderr\sale\models\price
 */
class PriceQuery extends ActiveQuery
{
    /**
     * @param string $base
     * @return PriceQuery
     */
    public function base($base = ModelHelper::STATUS_ACTIVE)
    {
        return $this->andWhere(['base' => $base]);
    }

    /**
     * {@inheritdoc}
     * @return PriceType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PriceType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}