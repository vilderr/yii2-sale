<?php

namespace vilderr\sale\models\price;

use yii\behaviors\TimestampBehavior;
use vilderr\main\models\users\User;
use vilderr\sale\behaviors\PriceTypeBehavior;
use vilderr\reference\behaviors\OwnerBehavior;
use DevGroup\TagDependencyHelper\CacheableActiveRecord;
use DevGroup\TagDependencyHelper\TagDependencyTrait;

/**
 * This is the model class for table "{{%catalog_price}}".
 *
 * @property int $id
 * @property string $name
 * @property string $base
 * @property int $sort
 * @property string $external_id
 * @property int $created_by
 * @property int $modified_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $createdBy
 * @property User $modifiedBy
 */
class PriceType extends \yii\db\ActiveRecord
{
    use TagDependencyTrait;

    protected static $basePriceCache;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sale_price_type}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'external_id'], 'required'],
            [['sort'], 'integer'],
            ['sort', 'default', 'value' => 100],
            [['name'], 'string', 'max' => 100],
            [['base'], 'string', 'max' => 1],
            [['external_id'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['modified_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modified_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'base' => 'Базовый',
            'sort' => 'Сортировка',
            'external_id' => 'Внешний код',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifiedBy()
    {
        return $this->hasOne(User::class, ['id' => 'modified_by']);
    }

    /**
     * @return PriceQuery
     */
    public static function find()
    {
        return new PriceQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'owner' => OwnerBehavior::class,
            'price' => PriceTypeBehavior::class,
            'cacheable' => CacheableActiveRecord::class
        ];
    }

    /**
     * @return PriceType|null
     */
    public static function getBasePrice()
    {
        if (!self::$basePriceCache && self::$basePriceCache !== null) {
            self::$basePriceCache = self::find()->base()->one();
        }

        return self::$basePriceCache;
    }
}
