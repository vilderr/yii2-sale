<?php

namespace vilderr\sale\models;

use vilderr\main\base\Error;
use vilderr\main\base\Result;
use Yii;
use vilderr\sale\models\price\PriceType;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%sale_product_price}}".
 *
 * @property int       $id
 * @property int       $product_id
 * @property int       $price_type
 * @property int       $price
 * @property string    $currency
 *
 * @property Product   $product
 * @property PriceType $priceType
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sale_product_price}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_type', 'price'], 'required'],
            [['product_id', 'price_type'], 'integer'],
            [['price'], 'number', 'message' => 'Значение цены должно быть числом'],
            [['currency'], 'string', 'max' => 3],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['price_type'], 'exist', 'skipOnError' => true, 'targetClass' => PriceType::class, 'targetAttribute' => ['price_type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'price_type' => 'Price Type',
            'price' => 'Price',
            'currency' => 'Currency'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceType()
    {
        return $this->hasOne(PriceType::class, ['id' => 'price_type']);
    }

    public function beforeValidate()
    {
        $this->price = str_replace(',', '.', $this->price);

        return parent::beforeValidate();
    }
}
