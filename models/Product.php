<?php

namespace vilderr\sale\models;

use vilderr\main\base\Error;
use vilderr\main\base\Result;
use vilderr\sale\behaviors\ProductBehavior;
use Yii;
use vilderr\sale\models\queries\ProductQuery;

/**
 * This is the model class for table "{{%sale_product}}".
 *
 * @property int     $id
 * @property double  $quantity
 * @property double  $quantity_reserved
 * @property string  $available
 * @property int     $measure
 * @property double  $weight
 * @property double  $width
 * @property double  $height
 * @property double  $length
 *
 * @property Price[] $prices
 * @mixin ProductBehavior
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sale_product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'measure'], 'integer'],
            [['quantity', 'quantity_reserved', 'weight', 'width', 'height', 'length'], 'default', 'value' => 0],
            [['quantity', 'quantity_reserved', 'weight', 'width', 'height', 'length'], 'normalizeDouble'],
            [['quantity', 'quantity_reserved', 'weight', 'width', 'height', 'length'], 'number'],
            [['available'], 'string', 'max' => 1],
            ['measure', 'exist', 'skipOnError' => true, 'targetClass' => Measure::class, 'targetAttribute' => ['measure' => 'id']]
        ];
    }

    public function getPrices()
    {
        return $this->hasMany(Price::class, ['product_id' => 'id'])->with('priceType')->indexBy('price_type');
    }

    public function normalizeDouble($attribute, $params)
    {
        $this->$attribute = str_replace(',', '.', $this->$attribute);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quantity' => 'Доступное количество',
            'quantity_reserved' => 'Зарезервированное количество',
            'available' => 'Available',
            'measure' => 'Единица измерения',
            'weight' => 'Вес (грамм)',
            'width' => 'Ширина (мм)',
            'height' => 'Высота (мм)',
            'length' => 'Длина (мм)',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \vilderr\sale\models\queries\ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            ProductBehavior::class
        ];
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->unlinkAll('prices', true);

            return true;
        }

        return true;
    }
}
