<?php

namespace vilderr\sale\models;

use vilderr\main\helpers\ModelHelper;
use Yii;
use vilderr\main\base\Result;
use vilderr\main\base\Error;

/**
 * This is the model class for table "{{%sale_measure}}".
 *
 * @property int    $id
 * @property int    $code
 * @property string $title
 * @property string $symbol_rus
 * @property string $symbol_intl
 * @property string $symbol_letter_intl
 * @property string $default
 */
class Measure extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sale_measure}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'title'], 'required'],
            [['code'], 'integer'],
            [['title'], 'string', 'max' => 500],
            [['symbol_rus', 'symbol_intl', 'symbol_letter_intl'], 'string', 'max' => 20],
            [['default'], 'string', 'max' => 1],
            [['code'], 'unique', 'message' => 'Единица измерения с кодом "{value}" уже существует'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'title' => 'Наименование',
            'symbol_rus' => 'Условное обозначение',
            'symbol_intl' => 'Международное обозначение',
            'symbol_letter_intl' => 'Международное буквенное обозначение',
            'default' => 'По умолчанию',
        ];
    }
}
