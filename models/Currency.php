<?php

namespace vilderr\sale\models;

use vilderr\reference\behaviors\OwnerBehavior;
use DevGroup\TagDependencyHelper\CacheableActiveRecord;
use DevGroup\TagDependencyHelper\TagDependencyTrait;
use vilderr\sale\behaviors\CurrencyBehavior;

/**
 * This is the model class for table "{{%sale_currency}}".
 *
 * @property string $id
 * @property string $base
 * @property int $amount_cnt
 * @property string $amount
 * @property int $sort
 * @property int $created_by
 * @property int $modified_by
 * @property string $current_base_rate
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sale_currency}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['amount_cnt', 'sort', 'created_by', 'modified_by'], 'integer'],
            [['amount', 'current_base_rate'], 'number'],
            ['id', 'match', 'pattern' => '/^[A-Z]+$/', 'message' => 'Используются только латинсике символы в верхнем регистре. Например: USD'],
            [['id'], 'string', 'max' => 3],
            [['base'], 'string', 'max' => 1],
            [['id'], 'unique', 'message' => 'Валюта "{value}" уже существует'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Валюта',
            'base' => 'Базовая',
            'amount_cnt' => 'Номинал',
            'amount' => 'Курс по умолчанию',
            'sort' => 'Сортировка',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'current_base_rate' => 'Current Base Rate',
        ];
    }

    public function behaviors()
    {
        return [
            'owner' => OwnerBehavior::class,
            'currency' => CurrencyBehavior::class
        ];
    }
}
