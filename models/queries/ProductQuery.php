<?php

namespace vilderr\sale\models\queries;

use vilderr\main\helpers\ModelHelper;

/**
 * This is the ActiveQuery class for [[\vilderr\sale\models\Product]].
 *
 * @see \vilderr\sale\models\Product
 */
class ProductQuery extends \yii\db\ActiveQuery
{
    public function available($available = ModelHelper::STATUS_ACTIVE)
    {
        return $this->andWhere(['available' => $available]);
    }

    /**
     * {@inheritdoc}
     * @return \vilderr\sale\models\Product[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \vilderr\sale\models\Product|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
