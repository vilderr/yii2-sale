<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use vilderr\sale\models\Measure;
use vilderr\sale\models\Product;

/**
 * @var Product      $model
 * @var array|null   $arMeasures
 * @var Measure|null $defaultMeasure
 */
?>
<table class="table table-sm dashboard-table no-border">
    <tr>
        <td class="text-right control-label control-label-sm"
            style="width:40%;"><?= $model->getAttributeLabel('quantity'); ?>:
        </td>
        <td><?= Html::textInput('product[quantity]', $model->quantity, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 30]); ?></td>
    </tr>
    <tr>
        <td class="text-right control-label control-label-sm"
            style="width:40%;"><?= $model->getAttributeLabel('quantity_reserved'); ?>:
        </td>
        <td><?= Html::textInput('product[quantity_reserved]', $model->quantity_reserved, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 30]); ?></td>
    </tr>
    <?php if (is_array($arMeasures) && !empty($arMeasures)): ?>
        <tr>
            <td class="text-right control-label control-label-sm"
                style="width:40%;"><?= $model->getAttributeLabel('measure'); ?>:
            </td>
            <td><?= Select2::widget([
                    'name' => 'product[measure]',
                    'data' => $arMeasures,
                    'value' => $model->measure ? $model->measure : $defaultMeasure->id,
                    'size' => Select2::SMALL,
                    'pluginOptions' => [
                        'allowClear' => false,
                        'placeholder' => 'Единица измерения'
                    ],
                ]); ?></td>
        </tr>
    <?php endif; ?>
    <tr class="heading">
        <td class="text-center" colspan="2">Вес и размеры</td>
    </tr>
    <tr>
        <td class="text-right control-label control-label-sm"
            style="width:40%;"><?= $model->getAttributeLabel('weight'); ?>:
        </td>
        <td><?= Html::textInput('product[weight]', $model->weight, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 30]); ?></td>
    </tr>
    <tr>
        <td class="text-right control-label control-label-sm"
            style="width:40%;"><?= $model->getAttributeLabel('length'); ?>:
        </td>
        <td><?= Html::textInput('product[length]', $model->length, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 30]); ?></td>
    </tr>
    <tr>
        <td class="text-right control-label control-label-sm"
            style="width:40%;"><?= $model->getAttributeLabel('width'); ?>:
        </td>
        <td><?= Html::textInput('product[width]', $model->width, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 30]); ?></td>
    </tr>
    <tr>
        <td class="text-right control-label control-label-sm"
            style="width:40%;"><?= $model->getAttributeLabel('height'); ?>:
        </td>
        <td><?= Html::textInput('product[height]', $model->height, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 30]); ?></td>
    </tr>
</table>
