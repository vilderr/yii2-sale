<?php

use yii\helpers\Html;
use vilderr\sale\models\price\PriceType;
use vilderr\sale\models\Price;

/**
 * @var PriceType[]  $priceTypes
 * @var Price[]      $prices
 */
?>
<?php if (!empty($priceTypes)): ?>
    <table class="table table-sm dashboard-table no-border">
        <?php foreach ($priceTypes as $pid => $priceType): ?>
            <tr>
                <td class="text-right control-label control-label-sm"
                    style="width: 40%; "><?= $priceType->name; ?><?= ($priceType->base === 'Y') ? ' (базовая)' : ''; ?></td>
                <td><?= Html::textInput('sale_price[' . $pid . '][price]', $prices[$pid]->price, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 30]); ?></td>
                <td><?= Html::hiddenInput('sale_price[' . $pid . '][currency]', 'RUB'); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
