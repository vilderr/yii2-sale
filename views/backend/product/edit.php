<?php

use yii\web\View;
use vilderr\sale\models\Product;
use vilderr\sale\models\Measure;
use vilderr\sale\models\price\PriceType;
use vilderr\sale\models\Price;

/**
 * @var View         $this
 * @var Product      $model
 * @var array|null   $arMeasures
 * @var Measure|null $defaultMeasure
 * @var PriceType[]  $priceTypes
 * @var Price[]      $prices
 */
?>
<?= \yii\bootstrap\Tabs::widget([
    'id' => 'product-edit-tabs',
    'options' => [
        'class' => 'inner-tabs'
    ],
    'items' => [
        [
            'label' => 'Цены',
            'content' => $this->renderPhpFile(Yii::getAlias('@vendor/vilderr/sale/views/backend/product/tabs/price.php'), ['priceTypes' => $priceTypes, 'prices' => $prices])
        ],
        [
            'label' => 'Параметры',
            'content' => $this->renderPhpFile(Yii::getAlias('@vendor/vilderr/sale/views/backend/product/tabs/params.php'), ['model' => $model, 'arMeasures' => $arMeasures, 'defaultMeasure' => $defaultMeasure])
        ]
    ]
]); ?>
