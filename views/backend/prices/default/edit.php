<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use vilderr\sale\models\price\PriceType;
use vilderr\sale\helpers\PriceHelper;

/**
 * @var $model PriceType
 */
?>
<div class="panel panel-default">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'class' => 'admin-form sale-price-form'
        ]
    ]); ?>
    <div class="panel-body">
        <table class="table table-sm dashboard-table no-border">
            <tr class="required-field">
                <td class="text-right control-label" style="width:40%;"><?= $model->getAttributeLabel('name'); ?>:</td>
                <td><?= Html::textInput('price_type[name]', $model->name, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 50]); ?></td>
            </tr>
            <tr>
                <td class="text-right control-label" style="width:40%;"><?= $model->getAttributeLabel('external_id'); ?>:</td>
                <td><?= Html::textInput('price_type[external_id]', $model->external_id, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 50]); ?></td>
            </tr>
            <tr>
                <td class="text-right control-label" style="width:40%;"><?= $model->getAttributeLabel('sort'); ?>:</td>
                <td><?= Html::textInput('price_type[sort]', $model->sort, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 5]); ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width:40%;"><?= $model->getAttributeLabel('base'); ?>:</td>
                <td><?= Html::checkbox('price_type[base]', $model->base === PriceHelper::STATUS_ACTIVE, ['value' => 'Y', 'uncheck' => 'N', 'label' => false]); ?></td>
            </tr>
        </table>
    </div>
    <div class="panel-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info btn-sm', 'name' => 'action', 'value' => 'save']) ?>
        <?= Html::submitButton(Yii::t('app', 'Apply'), ['class' => 'btn btn-outline-info btn-sm', 'name' => 'action', 'value' => 'apply']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
