<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use vilderr\sale\models\price\PriceType;
use vilderr\sale\collects\price\SearchCollection;
use vilderr\sale\helpers\PriceHelper;
use kartik\dynagrid\DynaGrid;

/**
 * @var $model SearchCollection
 * @var $data ActiveDataProvider
 */
?>
<?= DynaGrid::widget([
    'columns' => [
        [
            'attribute' => 'id',
            'order' => DynaGrid::ORDER_FIX_LEFT,
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'name',
            'value'     => function (PriceType $model) {
                return Html::a(Html::encode($model->name), Url::to(['update', 'id' => $model->id]));
            },
            'format'    => 'raw',
        ],
        [
            'attribute' => 'external_id'
        ],
        [
            'attribute' => 'sort',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'base',
            'filter' => PriceHelper::statusList(),
            'value' => function (PriceType $model) {
                return PriceHelper::statusLabel($model->base);
            },
            'format' => 'raw',
            'vAlign' => 'middle',
        ],
        [
            'class'          => 'kartik\grid\ActionColumn',
            'template'       => '<div class="btn-group">{update}{delete}</div>',
            'buttons'        => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="mdi mdi-pencil"></i>', ['update', 'id' => $model->id, 'returnUrl' => Url::current()], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<i class="mdi mdi-delete-empty"></i>', ['delete', 'id' => $model->id, 'returnUrl' => Url::current()], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete'), 'data-confirm' => 'Вы действительно хотите удвлить тип цены?']);
                },
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'options'        => [
                'width' => '90px',
            ],
            'order'          => DynaGrid::ORDER_FIX_RIGHT,
        ],
    ],
    'theme' => 'panel-default',
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'allowThemeSetting' => false,
    'gridOptions' => [
        'filterModel' => $model,
        'dataProvider' => $data,
        'hover' => true,
        'panel' => [
            'before' => Html::a('Новый тип цены', ['create'], ['class' => 'btn btn-info']),
            'after' => false,
        ],
    ],
    'options' => [
        'id' => 'unit-types-grid',
    ],
]); ?>
