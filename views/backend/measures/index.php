<?php

use yii\helpers\Html;
use yii\helpers\Url;
use vilderr\main\helpers\ModelHelper;
use vilderr\sale\models\Measure;
use yii\data\ActiveDataProvider;
use kartik\dynagrid\DynaGrid;

/**
 * @var ActiveDataProvider $data
 */
?>
<?= DynaGrid::widget([
    'columns' => [
        [
            'attribute' => 'id',
            'value' => function(Measure $model) {
                return Html::a(Html::encode($model->id), Url::to(['update', 'id' => $model->id]));
            },
            'format'    => 'raw',
            'order' => DynaGrid::ORDER_FIX_LEFT,
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'code',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'title',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'symbol_rus',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'symbol_intl',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'default',
            'value' => function (Measure $model) {
                return ModelHelper::statusLabel($model->default);
            },
            'format' => 'raw',
            'vAlign' => 'middle',
        ],
        [
            'class'          => 'kartik\grid\ActionColumn',
            'template'       => '<div class="btn-group">{update}{delete}</div>',
            'buttons'        => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="mdi mdi-pencil"></i>', ['update', 'id' => $model->id, 'returnUrl' => Url::current()], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<i class="mdi mdi-delete-empty"></i>', ['delete', 'id' => $model->id, 'returnUrl' => Url::current()], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete'), 'data-confirm' => 'Вы действительно хотите удалить единицу измерения?']);
                },
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'options'        => [
                'width' => '90px',
            ],
            'order'          => DynaGrid::ORDER_FIX_RIGHT,
        ],
    ],
    'theme' => 'panel-default',
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'allowThemeSetting' => false,
    'gridOptions' => [
        'dataProvider' => $data,
        'hover' => true,
        'panel' => [
            'before' => Html::a('Добавить', ['create'], ['class' => 'btn btn-info']),
            'after' => false,
        ],
    ],
    'options' => [
        'id' => 'sale-measure-list',
    ],
]); ?>
