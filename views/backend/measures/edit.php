<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use vilderr\sale\models\Measure;
use vilderr\main\helpers\ModelHelper;

/**
 * @var Measure $model
 */
?>
<div class="panel panel-default">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'class' => 'admin-form sale-measure-form'
        ]
    ]); ?>
    <div class="panel-body">
        <table class="table table-sm dashboard-table no-border">
            <tr>
                <td class="text-right" style="width:40%;"><?= $model->getAttributeLabel('default'); ?>:</td>
                <td><?= Html::checkbox('measure[default]', $model->default === ModelHelper::STATUS_ACTIVE, ['value' => 'Y', 'uncheck' => 'N', 'label' => false]); ?></td>
            </tr>
            <tr class="required-field">
                <td class="text-right control-label"
                    style="width:40%;"><?= $model->getAttributeLabel('code'); ?>:
                </td>
                <td>
                    <?= Html::textInput('measure[code]', $model->code, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 5]); ?>
                </td>
            </tr>
            <tr class="required-field">
                <td class="text-right control-label"
                    style="width:40%;"><?= $model->getAttributeLabel('title'); ?>:
                </td>
                <td>
                    <?= Html::textInput('measure[title]', $model->title, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 50]); ?>
                </td>
            </tr>
            <tr>
                <td class="text-right control-label"
                    style="width:40%;"><?= $model->getAttributeLabel('symbol_rus'); ?>:
                </td>
                <td>
                    <?= Html::textInput('measure[symbol_rus]', $model->symbol_rus, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 10]); ?>
                </td>
            </tr>
            <tr>
                <td class="text-right control-label"
                    style="width:40%;"><?= $model->getAttributeLabel('symbol_intl'); ?>:
                </td>
                <td>
                    <?= Html::textInput('measure[symbol_intl]', $model->symbol_intl, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 10]); ?>
                </td>
            </tr>
            <tr>
                <td class="text-right control-label"
                    style="width:40%;"><?= $model->getAttributeLabel('symbol_letter_intl'); ?>:
                </td>
                <td>
                    <?= Html::textInput('measure[symbol_letter_intl]', $model->symbol_letter_intl, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 10]); ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="panel-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info btn-sm', 'name' => 'action', 'value' => 'save']) ?>
        <?= Html::submitButton(Yii::t('app', 'Apply'), ['class' => 'btn btn-outline-info btn-sm', 'name' => 'action', 'value' => 'apply']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
