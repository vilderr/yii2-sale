<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use vilderr\sale\models\Currency;
use vilderr\main\helpers\ModelHelper;

/**
 * @var $model Currency
 */
?>
<div class="panel panel-default">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'class' => 'admin-form sale-currency-form'
        ]
    ]); ?>
    <div class="panel-body">
        <table class="table table-sm dashboard-table no-border">
            <tr class="required-field">
                <td class="text-right control-label" style="width:40%;"><?= $model->getAttributeLabel('id'); ?>:</td>
                <td><?= Html::textInput('currency[id]', $model->id, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 5]); ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width:40%;"><?= $model->getAttributeLabel('base'); ?>:</td>
                <td><?= Html::checkbox('currency[base]', $model->base === ModelHelper::STATUS_ACTIVE, ['value' => 'Y', 'uncheck' => 'N', 'label' => false]); ?></td>
            </tr>
            <tr>
                <td class="text-right control-label" style="width:40%;"><?= $model->getAttributeLabel('amount_cnt'); ?>:</td>
                <td><?= Html::textInput('currency[amount_cnt]', $model->amount_cnt, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 5]); ?></td>
            </tr>
            <tr>
                <td class="text-right control-label" style="width:40%;"><?= $model->getAttributeLabel('amount'); ?>:</td>
                <td><?= Html::textInput('currency[amount]', $model->amount, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 5]); ?></td>
            </tr>
            <tr>
                <td class="text-right control-label" style="width:40%;"><?= $model->getAttributeLabel('sort'); ?>:</td>
                <td><?= Html::textInput('currency[sort]', $model->sort, ['class' => 'form-control form-control-sm', 'style' => 'width:auto;', 'size' => 5]); ?></td>
            </tr>
        </table>
    </div>
    <div class="panel-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-info btn-sm', 'name' => 'action', 'value' => 'save']) ?>
        <?= Html::submitButton(Yii::t('app', 'Apply'), ['class' => 'btn btn-outline-info btn-sm', 'name' => 'action', 'value' => 'apply']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
