<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use vilderr\sale\models\Currency;
use kartik\dynagrid\DynaGrid;
use vilderr\sale\helpers\PriceHelper;

/**
 * @var $data ActiveDataProvider
 */
?>
<?= DynaGrid::widget([
    'columns' => [
        [
            'attribute' => 'id',
            'value' => function(Currency $model) {
                return Html::a(Html::encode($model->id), Url::to(['update', 'id' => $model->id]));
            },
            'format'    => 'raw',
            'order' => DynaGrid::ORDER_FIX_LEFT,
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'sort',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'amount_cnt',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'amount',
            'vAlign' => 'middle'
        ],
        [
            'attribute' => 'base',
            'filter' => PriceHelper::statusList(),
            'value' => function (Currency $model) {
                return PriceHelper::statusLabel($model->base);
            },
            'format' => 'raw',
            'vAlign' => 'middle',
        ],
        [
            'class'          => 'kartik\grid\ActionColumn',
            'template'       => '<div class="btn-group">{update}{delete}</div>',
            'buttons'        => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="mdi mdi-pencil"></i>', ['update', 'id' => $model->id, 'returnUrl' => Url::current()], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<i class="mdi mdi-delete-empty"></i>', ['delete', 'id' => $model->id, 'returnUrl' => Url::current()], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete'), 'data-confirm' => 'Вы действительно хотите удалить валюту?']);
                },
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'options'        => [
                'width' => '90px',
            ],
            'order'          => DynaGrid::ORDER_FIX_RIGHT,
        ],
    ],
    'theme' => 'panel-default',
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'allowThemeSetting' => false,
    'gridOptions' => [
        'dataProvider' => $data,
        'hover' => true,
        'panel' => [
            'before' => Html::a('Новая валюта', ['create'], ['class' => 'btn btn-info']),
            'after' => false,
        ],
    ],
    'options' => [
        'id' => 'unit-types-grid',
    ],
]); ?>
